

module MyVagrantPlugin
  class Config < Vagrant.plugin("2","config")
    attr_accessor :value
    def initialize
      super
      @value = UNSET_VALUE
    end
    
    def finalize!
      @value = nil if @value = UNSET_VALUE
    end
    
    def merge
      #overrides the default merge behaviour of configuration for this plugin
    end
    
    def validate(machine)
        errors = []
        if !@value
            errors << "Value must be set!"
        end
        
        return {"my_key" => errors}
    end
  end
     
end
