#
module MyVagrantPlugin
  class Command < Vagrant.plugin("2","command")
    def execute
      command = "grep MemFree /proc/meminfo | awk '{print $2}'"
      options = {}
      opts = OptionParser.new do |o|
        o.banner = "Usage: vagrant free-memory [--help] [--format]"
        o.on("-f", "--format FORMAT", "Choose format") do |v|
          options[:format] = v
        end
      end
      
      argv = parse_options(opts)
      puts argv.inspect
      puts options.inspect
      with_target_vms(argv) do |machine|
                
        if machine.state.id != :running
          @env.ui.error("#{machine.name}: Machine must be running.")
          next
        end
        
        machine.communicate.execute(command) do |type, data|
          @env.ui.info("#{machine.name}: #{data}")
        end
      end
      
      return 0
    
    end
  end
end
