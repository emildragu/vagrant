require "my_vagrant_plugin/version"

module MyVagrantPlugin
  # Your code goes here...
  class Plugin < Vagrant.plugin("2")
    name "My vagrant plugin"
    command "free-memory" do
      require_relative "my_vagrant_plugin/command"
      Command
    end
    config "my_key" do
      require_relative "my_vagrant_plugin/config"
      Config
    end
  end
end
